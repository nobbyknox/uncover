# Uncover

_Uncover_ is a minimalist web-based presentation framework. Write your slides as `section` elements and _Uncover_ will give you just enough framework to present your slide presentation.

## Getting Started

Clone the repository or download the source code. Then, copy the `css/plain.css` and `js/nav.js` files into your project. Use the `template.html` file as a starting point. Below is a sample:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href=css/plain.css rel=stylesheet>
    <script src="js/nav.js" type="text/javascript"></script>
    <title>Presentation Title</title>
</head>
<body>
    <section>
        <h1>Slide 1</h1>
        <h2>Intro</h2>

        <p>This presentation is going to be awesome!</p>
    </section>
</body>
</html>
```

## Frequently Asked Questions

### Why not use Reveal.js?

In truth, I used Reveal.js for a long time. It was my go-to presentation tool that provided a web-based platform for delivering presentations and that sat very well with me. Well enough, until I got frustrated with regards to the following:

1. Versioning my presentations became too much of a problem in respect of the Reveal.js assets. Do I version it too, or not? If not, how do I quickly setup and run a presentation from scratch? With *Uncover*, you just version the style and navigation files along with your project. The two files are small enough not to even care about.
1. The layout, or visible real estate that Reveal.js allows you to work with, became very restrictive to me. The bullet/numbered lists, for instance, never looked right to me.
1. The fact that everything was centered was also not always to my liking.
1. Presentation hardware sometimes requires me to zoom the font in or out. Something that Reveal.js does not do very well.

These frustrations led me to create *Uncover*.

### How do I print my presentation?

Press the `a` key to show all your slides and then print the page. Afterwards you may press `a` again to go back to slide view mode.
